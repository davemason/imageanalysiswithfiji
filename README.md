### Image Analysis with Fiji Workshop ###
A set of slides and training material written in HTML/CSS with the [Reveal.js](https://github.com/hakimel/reveal.js) framework to support a workshop on Image Analysis with Fiji for the [2018 Centre for Cell Imaging Workshop](http://cci.liv.ac.uk/2018_workshop.html).

A copy of the slides are currently hosted here: [http://pcwww.liv.ac.uk/~cci/ia.html](http://pcwww.liv.ac.uk/~cci/ia.html)

### Licencing and reuse ###
The [Reveal.js](https://github.com/hakimel/reveal.js) framework is used under the terms of the MIT licence.

The slide content and images (see exception below) are provided under a [CC-BY licence](https://creativecommons.org/licenses/by/4.0/). You may reuse the contents for any purpose in any way you please as long as you attribute the source material (see link for other licence terms).

### Acknowledgement ###
Written by [Dave Mason](https://mas.to/@dn_mason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk). If you are interested in Dave running this workshop for your group, core facility or meeting, please get in touch.